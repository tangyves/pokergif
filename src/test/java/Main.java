import com.frienmo.poker.HHParserException;
import com.frienmo.poker.HandHistory;
import com.frienmo.poker.PokerStarParser;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        try {
            ArrayList<String> lines = (ArrayList<String>) FileUtils.readLines(new File("src/test/1.txt"),"UTF-8");
            HandHistory hh = new PokerStarParser().parse(lines);
//            System.out.println(hh.getSite());
//            System.out.println(hh.getPreflopActions().size());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (HHParserException e) {
            e.printStackTrace();
        }
    }
}
