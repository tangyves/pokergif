package com.frienmo.poker;

/**
 * Created by 4 on 2016-01-31.
 */
public enum Site {
    POKERSTARS("PokerStars");

    private final String name;
    private Site(String s) {
        name = s;
    }
    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }
}
