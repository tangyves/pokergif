package com.frienmo.poker;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 4 on 2016-01-29.
 */
public class HandHistory {



    private HashMap<Integer,Player> posPlayerMap = new HashMap<>();
    private HashMap<String,Player> nameMap = new HashMap<>();
    private int dealerPos;
    private int playerNumber = 0;

    private ArrayList<String> originalText;

    private Site site;
    private String initIdBySite;
    private String tableName;
    private String currency;
    private double smallBlind;
    private double bigBlind;
    private String time;

    private List<Action> preflopActions = new ArrayList<>();
    private List<Action> flopActions = new ArrayList<>();
    private List<Action> turnActions = new ArrayList<>();
    private List<Action> riverActions = new ArrayList<>();


    char[] cards = new char[4];
    char[] flop = new char[6];
    char[] turn = new char[2];
    char[] river = new char[2];

    private BigDecimal totalPot;
    private BigDecimal rake;

    public HandHistory() {
        // test only
    }

    public HandHistory(Site site, ArrayList<String> originalText) {
        this.site = site;
        this.originalText = originalText;
    }

    public HandHistory(ArrayList<String> lines) throws HHParserException {
//        this.originalText = lines;
//        int pointer = 0;
//        String firstLine = lines.get(pointer);
//        parseFirstLine(firstLine);
//        ++pointer;
//        String secondLine = lines.get(pointer);
//        parseSecondLine(secondLine);
//        ++pointer;
//        pointer = parseInitState();
//        for (String l : lines) {
//            System.out.println(l);
//        }
    }

    public HashMap<Integer, Player> getPosPlayerMap() {
        return posPlayerMap;
    }


    public HashMap<String, Player> getNameMap() {
        return nameMap;
    }

    public void setNameMap(HashMap<String, Player> nameMap) {
        this.nameMap = nameMap;
    }

    public int getDealerPos() {
        return dealerPos;
    }

    public Player getPlayer(int pos) {
        if(pos > playerNumber)
            throw new RuntimeException("pos > total number");
        return this.posPlayerMap.get(pos);
    }

    public void setDealerPos(int dealerPos) {
        this.dealerPos = dealerPos;
    }

    public int getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    public ArrayList<String> getOriginalText() {
        return originalText;
    }

    public void setOriginalText(ArrayList<String> originalText) {
        this.originalText = originalText;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public String getInitIdBySite() {
        return initIdBySite;
    }

    public void setInitIdBySite(String initIdBySite) {
        this.initIdBySite = initIdBySite;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getSmallBlind() {
        return smallBlind;
    }

    public void setSmallBlind(double smallBlind) {
        this.smallBlind = smallBlind;
    }

    public double getBigBlind() {
        return bigBlind;
    }

    public void setBigBlind(double bigBlind) {
        this.bigBlind = bigBlind;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<Action> getPreflopActions() {
        return preflopActions;
    }

    public List<Action> getFlopActions() {
        return flopActions;
    }

    public List<Action> getTurnActions() {
        return turnActions;
    }

    public List<Action> getRiverActions() {
        return riverActions;
    }

    public void addPreflopAction(Action action) {
        preflopActions.add(action);
    }

    public void addFlopAction(Action action) {
        flopActions.add(action);
    }

    public void addTurnAction(Action action) {
        turnActions.add(action);
    }

    public void addRiverAction(Action action) {
        riverActions.add(action);
    }

    public void addPlayer(Player p) {
        playerNumber++;
        this.posPlayerMap.put(p.getPosition(),p);
        this.nameMap.put(p.getName(),p);
    }

    public Player getPlayer(String name) {
        return nameMap.get(name);
    }

    public void setHeroCards(char[] cards) {
        this.cards = cards.clone();
    }

    public char[] getHeroCards() {
        return this.cards.clone();
    }

    public char[] getFlop() {
        return flop;
    }

    public void setFlop(char[] flop) {
        this.flop = flop.clone();
    }

    public char[] getTurn() {
        return turn;
    }

    public void setTurn(char[] turn) {
        this.turn = turn.clone();
    }

    public char[] getRiver() {
        return river;
    }

    public void setRiver(char[] river) {
        this.river = river.clone();
    }

    public BigDecimal getRake() {
        return rake;
    }

    public void setRake(BigDecimal rake) {
        this.rake = rake;
    }

    public BigDecimal getTotalPot() {
        return totalPot;
    }

    public void setTotalPot(BigDecimal totalPot) {
        this.totalPot = totalPot;
    }
}
