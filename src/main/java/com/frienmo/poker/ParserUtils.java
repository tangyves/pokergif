package com.frienmo.poker;

import java.math.BigDecimal;

/**
 * Created by 4 on 2016-02-11.
 */
public class ParserUtils {
    public static String detectCurrency(String firstLine) {
        if (firstLine.indexOf("€") < firstLine.length())
            return "EUR";
        if (firstLine.indexOf("$") < firstLine.length())
            return "USD";
        return null;
    }

    public static BigDecimal removeCurrencySymbolToCents(String money) {
        String s = money.substring(1,money.length());
        BigDecimal i = new BigDecimal(s);
        return i;
    }
}
