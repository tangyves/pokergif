package com.frienmo.poker.gif;

import com.frienmo.poker.gif.GifMaker;

import java.io.File;
import java.io.IOException;

/**
 * Created by yichen on 2/5/16.
 */
public class GifMaker_exp {
    public static void main(String[] args) {
        File f = new File("test.gif");
        GifMaker gifMaker = new GifMaker(f);
        try {
            gifMaker.generate();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
