package com.frienmo.poker.gif;

import java.util.HashMap;

/**
 * Created by 4 on 2016-02-07.
 */
public class PositionUtils {
    private int nbPlayer;
    private int height;
    private int width;

    HashMap<Integer,int[]> cors;

    private static final float DEFAULT_TABLE_HEIGHT_RATIO = 0.7f;
    private static final float DEFAULT_TABLE_WIDTH_RATIO = 0.8f;

    private static final float DEFAULT_PLAYER_CIRCLE_DIAMETER_RATIO = (1/6f);

    private static final float CARD_WIDTH_RATIO = 0.1f;
    private static final float CARD_HEIGHT = 1.5f;

    private static final float DEFAULT_CARD_WIDTH_CIRCLE_RATIO = 0.5f;
    private static final float DEFAULT_CARD_HEIGHT_WIDTH_RATIO = 1.5f;
    private static final int DEFAULT_CARDS_MARGIN = 5;

    private static final float DEFAULT_BUTTON_SIZE = 0.7f;
    private int buttonSize;

    public int getPlayerCircleDiameter() {
        return playerCircleDiameter;
    }


    private int playerCircleDiameter;
    private int cardWidth;
    private int cardHeight;

    public PositionUtils(int height, int width, int nbPlayer) {
        this.height = height;
        this.width = width;
        this.nbPlayer = nbPlayer;

        this.playerCircleDiameter = (int) (DEFAULT_PLAYER_CIRCLE_DIAMETER_RATIO * height);
        this.cardWidth = (int) (this.playerCircleDiameter * DEFAULT_CARD_WIDTH_CIRCLE_RATIO);
        this.cardHeight = (int) (this.cardWidth * DEFAULT_CARD_HEIGHT_WIDTH_RATIO);

        this.buttonSize = (int) (this.cardWidth * DEFAULT_BUTTON_SIZE);
        // init positions
        initPositions();
    }

    public int getCardWidth() {
        return cardWidth;
    }

    public int getCardHeight() {
        return cardHeight;
    }

    public int getButtonSize() {
        return buttonSize;
    }

    private void initPositions() {
        int radius = this.playerCircleDiameter / 2;
        int x3 = (int) (width / 3);
        int x8 = (int) (width / 9);
        int y2 = (int) (height / 2);
//        int y6 = (int) (height / 6);
//        int y7 = (int) (height / 7);
        int y9 = (int) (height / 9);
        cors = new HashMap<>();
        for(int i = 1; i <=nbPlayer; i++) {
            cors.put(i, new int[2]);
        }
        if(nbPlayer == 6) {
            cors.get(1)[0] = x3 - radius;
            cors.get(1)[1] = y9 - radius;
            cors.get(2)[0] = x3 * 2 - radius;
            cors.get(2)[1] = y9 - radius;
            cors.get(3)[0] = x8 * 8 - radius;
            cors.get(3)[1] = y2 - radius;
            cors.get(4)[0] = x3 * 2 - radius;
            cors.get(4)[1] = y9 * 8 - radius;
            cors.get(5)[0] = x3 - radius;
            cors.get(5)[1] = y9 * 8 - radius;
            cors.get(6)[0] = x8 - radius;
            cors.get(6)[1] = y2 - radius;
        }
    }

    public int[] getPlayerCoordinate(int i){
        return cors.get(i).clone();
    }

    public int[] getPlayerNameCoordiante(int i) {
        int[] cors = getPlayerCoordinate(i).clone();
        cors[1] = cors[1] + playerCircleDiameter/3;
        return cors;
    }

    public int[] getPlayerChipCoordiante(int i) {
        int[] cors = getPlayerCoordinate(i).clone();
        cors[0] = cors[0] + playerCircleDiameter/3;
        cors[1] = cors[1] + playerCircleDiameter/2 + 4;
        return cors;
    }

    public int[] getPlayerBetChipCoordinate(int i) {
        if(i < i || i > nbPlayer) {
            return null;
        }
        int[] cors = new int[2];
        if(nbPlayer == 6) {
            // x
            int x = getPlayerCardsCoordinate(i)[0];
            if(i!=3 && i!=6) {
                cors[0] = x + 5;
//                cors[1] = getPlayerCoordinate(i)[1];
            } else if(i ==3) {
                cors[0] = x;
            } else {
                cors[0] = x;
            }
            // y
            int y = getPlayerCardsCoordinate(i)[1];
            if(i==1 || i ==2) {
                cors[1] = y + cardHeight + 10;
            } else if(i ==3 || i ==6) {
                cors[1] = y + cardHeight + 10;
            } else {
                cors[1] = y - 8;
            }
        }
        return cors;
    }

    public int[] getButtonCoordiante(int i) {
        int[] cors = getPlayerCoordinate(i).clone();
        cors[0] = cors[0] + playerCircleDiameter/3;
        cors[1] = cors[1] + playerCircleDiameter*4/5 + 5;
        return cors;
    }

    public int[] getPlayerCardsCoordinate(int i ) {
        if(i < 1 || i > nbPlayer) {
            return null;
        }
        int[] cors = new int[4];
        if(nbPlayer == 6) {
            // x
            int x = getPlayerCoordinate(i)[0];
            if(i!=3 && i!=6) {
                cors[0] = x;
//                cors[1] = getPlayerCoordinate(i)[1];
            } else if(i ==3) {
                cors[0] = x - 2*cardWidth;
            } else {
                cors[0] = x + playerCircleDiameter;
            }
            cors[2] = cors[0] + cardWidth + DEFAULT_CARDS_MARGIN;
            // y
            int y = getPlayerCoordinate(i)[1];
            if(i==1 || i ==2) {
                cors[1] = y + playerCircleDiameter;
            } else if(i ==3 || i ==6) {
                cors[1] = (height - cardHeight) /2;
            } else {
                cors[1] = y - cardHeight;
            }
            cors[3] = cors[1];
        }
        return cors;
    }

    /**
     *
     * @return 0=y cors
     */
    public int[] getCommunityCardsCoordinate() {
        int[] cors = new int[6];
        cors[0] = (int) (height / 2 - cardHeight / 2);
        cors[1] = (int) (width / 2 - 2.5 * cardWidth - 2 * DEFAULT_CARDS_MARGIN);
        for(int i = 2; i <=5; i++) {
            cors[i] = cors[i-1] + cardWidth + DEFAULT_CARDS_MARGIN;
        }
        return cors;
    }

    public int[] getPotCoordinate() {
        int[] cors = new int[2];
        cors[0] = width/2;
        cors[1] = height/4;
        return cors;
    }
}
