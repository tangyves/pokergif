package com.frienmo.poker.gif;

import java.awt.*;

/**
 * Created by 4 on 2016-02-20.
 */
public class GifConstants {

    public static final float DEFAULT_PLAYER_CIRCLE_DIAMETER_RATIO = (1/6f);

    public static final Color DEFAULT_TABLE_COLOR = new Color(16,103,21);
    public static final Color DEFAULT_PLAYER_COLOR = Color.gray;
    public static final Color DEFAULT_PLAYERINFO_COLOR = Color.white;
}
