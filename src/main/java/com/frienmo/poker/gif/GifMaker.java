package com.frienmo.poker.gif;

import com.frienmo.poker.*;
import org.apache.commons.io.FileUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * Created by yichen on 2/5/16.
 */
public class GifMaker {


    private static final int DEFAULT_WIDTH = 600;
    private static final int DEFAULT_HEIGHT = 300;

    private static final float DEFAULT_TABLE_HEIGHT_RATIO = 0.7f;
    private static final float DEFAULT_TABLE_WIDTH_RATIO = 0.8f;



    private final PositionUtils positionUtils;

    private int width;
    private int height;
    private Color tableColor;


    File destFile;
    BufferedImage image;
    BufferedImage tmpImage;
    Graphics g;

    HandHistory hand;

    Map<String, PlayerInGif> playerMap = new HashMap<>();
    static Map<String,Integer> frameInter = new HashMap<>();
    static {
//        System.out.println("static");
        try {
            List<String> lines = FileUtils.readLines(new File("interval.properties"),"UTF-8");
            for(String line : lines) {
                String[] p = line.split("=");
                frameInter.put(p[0],Integer.valueOf(p[1]));
            }
//            System.out.println(lines);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    AnimatedGifEncoder animatedGifEncoder;

    public GifMaker(File destFile) {
        this(destFile,DEFAULT_WIDTH,DEFAULT_HEIGHT,GifConstants.DEFAULT_TABLE_COLOR);
    }

    public GifMaker(File destFile, int width, int height, Color tableColor) {
        this.destFile = destFile;
        ArrayList<String> lines = null;
        try {
            lines = (ArrayList<String>) FileUtils.readLines(new File("src/test/1.txt"), "UTF-8");
            this.hand = new PokerStarParser().parse(lines);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (HHParserException e) {
            e.printStackTrace();
        }

        this.width = width;
        this.height = height;
        this.tableColor = tableColor;
        image = new BufferedImage(width, height,BufferedImage.TYPE_4BYTE_ABGR);
        g = image.getGraphics();

        positionUtils = new PositionUtils(height, width, hand.getPlayerNumber());
    }

    public void generate() throws IOException {
        int gifIdx = 0;
        System.out.println(hand.getPlayerNumber());
        animatedGifEncoder = new AnimatedGifEncoder();
        animatedGifEncoder.start(destFile);
        animatedGifEncoder.setRepeat(0);
        drawTable();
//        for (int i=1; i <=6; i++) {
//            drawCards(i);
//        }

        for(int i =1; i <= hand.getPlayerNumber(); i++) {
            Player p = hand.getPlayer(i);
//            PlayerInGif pig = (PlayerInGif) hand.getPlayer(i);
            PlayerInGif pig = new PlayerInGif(p.getName(),p.getInitChip(),p.getPosition(),
                    p.getCards(),p.isCardAvailable(),positionUtils,g);
            playerMap.put(p.getName(),pig);
            pig.sit();
//            pig.doAction(null);
            if (pig.getPosition() == hand.getDealerPos())
                pig.putButton();
        }
//        boolean isPostBlind = true;
        List<Action> preflop = hand.getPreflopActions();
        for(Action action : preflop) {
            String name = action.player.getName();
            PlayerInGif pig = playerMap.get(name);
            if(action.type == Action.Type.POST) {
                // blind as init state
                pig.doAction(action);
            } else {
                if(gifIdx == 0) {
                    // init state with blind
                    ++gifIdx;
                    addImgToGifAndSave(gifIdx,"");
                }
                pig.doAction(action);
                ++gifIdx;
                addImgToGifAndSave(gifIdx,action.type.toString());
//                break;
            }
        }

        collectBetChipToPot();
        drawFlopCards();
        ++gifIdx;
        addImgToGifAndSave(gifIdx,"");

        List<Action> postFlop = hand.getFlopActions();
        for(Action action : postFlop) {
            String name = action.player.getName();
            PlayerInGif pig = playerMap.get(name);
            pig.doAction(action);
            ++gifIdx;
            addImgToGifAndSave(gifIdx,action.type.toString());
        }

        collectBetChipToPot();
        drawCard(4);
        ++gifIdx;
        addImgToGifAndSave(gifIdx,"");

        List<Action> turnAction = hand.getTurnActions();
        for(Action action : turnAction) {
            String name = action.player.getName();
            PlayerInGif pig = playerMap.get(name);
            pig.doAction(action);
            ++gifIdx;
            addImgToGifAndSave(gifIdx,action.type.toString());
        }

        collectBetChipToPot();
        drawCard(5);
        ++gifIdx;
        addImgToGifAndSave(gifIdx,"");

        List<Action> riverAction = hand.getRiverActions();
        for(Action action : riverAction) {
            String name = action.player.getName();
            PlayerInGif pig = playerMap.get(name);
            pig.doAction(action);
            ++gifIdx;
            addImgToGifAndSave(gifIdx,action.type.toString());
        }

        animatedGifEncoder.finish();
        System.out.println("done");
    }

    private void addImgToGifAndSave(int i, String actionName) throws IOException {
        ImageIO.write(image, "gif", new File("test" + i + ".gif"));
//        if(tmpImage == null)

        Integer j;
        j = frameInter.get(actionName);
        if(j == null) {
            j = 700;
        }
        animatedGifEncoder.setDelay(j);
        if(tmpImage !=null)
            animatedGifEncoder.addFrame(tmpImage);
        tmpImage = deepCopy(image);
    }

    private void drawFlopCards() {
        int[] cors = positionUtils.getCommunityCardsCoordinate();
        char[] cards = hand.getFlop();
        Color c = g.getColor();
        g.setColor(Color.white);
        g.fillRect(cors[1], cors[0], positionUtils.getCardWidth(), positionUtils.getCardHeight());
        g.fillRect(cors[2], cors[0], positionUtils.getCardWidth(), positionUtils.getCardHeight());
        g.fillRect(cors[3], cors[0], positionUtils.getCardWidth(), positionUtils.getCardHeight());
        PlayerInGif.drawCardValue(cors[1],cors[0],cards[0],cards[1],g);
        PlayerInGif.drawCardValue(cors[2], cors[0], cards[2], cards[3], g);
        PlayerInGif.drawCardValue(cors[3], cors[0], cards[4], cards[5], g);
        g.setColor(c);
    }

    private void drawCard(int i) {
        int[] cors = positionUtils.getCommunityCardsCoordinate();
        char[] card = hand.getTurn();
        Color c = g.getColor();
        g.fillRect(cors[i], cors[0], positionUtils.getCardWidth(), positionUtils.getCardHeight());
        PlayerInGif.drawCardValue(cors[i],cors[0],card[0],card[1],g);
        g.setColor(c);
    }

    private void collectBetChipToPot() {
//        System.out.println("colect bet chip");
        for(Map.Entry<String,PlayerInGif> mapentry : playerMap.entrySet()) {
            PlayerInGif pig = mapentry.getValue();
            if(!pig.isFold) {
//                pig.cleanBetChip();
                pig.collectBetChip();
            }
        }
    }

    private void drawCommunityCards() {
        int cors[] = positionUtils.getCommunityCardsCoordinate();
        g.setColor(Color.white);
        for(int i = 1; i <=5; i++) {
            g.fillRect(cors[i],cors[0],positionUtils.getCardWidth(),positionUtils.getCardHeight());
        }
    }

    private void drawTable() {
        g.setColor(tableColor);
//        g.fillArc(30, 50, 30, 80, 0, 90);
//        g.setColor(Color.red);
//        g.fillArc(60, 70, 30, 50, 0, -90);
//        g.setColor(Color.blue);
//        g.fillArc(80, 10, 100, 180, 90, 180);
        int upperLeftCornerX = (int) ((1 - DEFAULT_TABLE_WIDTH_RATIO) / 2 * width);
        int upperLeftCornerY = (int) ((1 - DEFAULT_TABLE_HEIGHT_RATIO) / 2 * height);
        int tableHeight = (int) (DEFAULT_TABLE_HEIGHT_RATIO * height); // banqiu zhijin
        int tableWidth = (int) (DEFAULT_TABLE_WIDTH_RATIO * width);
        // left table circle
        g.fillArc(upperLeftCornerX, upperLeftCornerY, tableHeight, tableHeight,90,180);

        int upperLeftCornerX2 = (int) (width - upperLeftCornerX - tableHeight);
        g.setColor(tableColor);
        // right table circle
        g.fillArc(upperLeftCornerX2,upperLeftCornerY, tableHeight, tableHeight,-90,180);

        int recX = (int) (upperLeftCornerX + tableHeight / 2);
        g.fillRect(recX, upperLeftCornerY,tableWidth - tableHeight + 2, tableHeight);

        //pot
        g.setColor(Color.WHITE);
        g.drawString("Pot:",tableWidth/2,tableHeight/4-10);
    }
    static BufferedImage deepCopy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }
}
