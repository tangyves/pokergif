package com.frienmo.poker.gif;

import com.frienmo.poker.Action;
import com.frienmo.poker.Player;
import org.apache.log4j.Logger;

import java.awt.*;
import java.math.BigDecimal;

/**
 * Created by 4 on 2016-02-19.
 */
public class PlayerInGif extends Player {

    private final static Logger log = Logger.getLogger(PlayerInGif.class);

    BigDecimal handChip;
    static PositionUtils positionUtils;
    static Graphics g;

    // pos
    int x;
    int y;
    int[] nameCors;
    int[] chipCors;
    int[] betChipCors = null;
    int[] cardsCors;

    static BigDecimal pot = new BigDecimal(0);
    static int[] potCors;

    BigDecimal betChip = null;
    String actionName = "";
    boolean checked = false;

    public boolean isFold = false;


    public PlayerInGif(String name, BigDecimal initChip, int position, char[] cards, boolean cardAvailable, PositionUtils positionUtils, Graphics g) {
        super(name, initChip, position);
        this.handChip = initChip;
        this.positionUtils = positionUtils;
        // calculate pos
        calculatePosition();
        this.g = g;
        this.cards = cards;
        this.cardAvailable = cardAvailable;
    }

    private void calculatePosition() {
        this.x = positionUtils.getPlayerCoordinate(this.position)[0];
        this.y = positionUtils.getPlayerCoordinate(this.position)[1];
        this.nameCors = positionUtils.getPlayerNameCoordiante(this.position);
        this.chipCors = positionUtils.getPlayerChipCoordiante(this.position);
        this.cardsCors = positionUtils.getPlayerCardsCoordinate(this.position);
        // TODO init
        potCors = positionUtils.getPotCoordinate();
        this.betChipCors = positionUtils.getPlayerBetChipCoordinate(this.position);
    }

    public void sit() {
        System.out.println(this.name + " sit");
        Color tmp = g.getColor();
        int diameter = positionUtils.getPlayerCircleDiameter();
        g.setColor(GifConstants.DEFAULT_PLAYER_COLOR);
        g.fillOval(x,y,diameter,diameter);
        g.setColor(GifConstants.DEFAULT_PLAYERINFO_COLOR);
        char[] name = this.name.toCharArray();
        int l = name.length;
        g.drawChars(name, 0, l, nameCors[0], nameCors[1]);
        drawHandChip();
        drawCards();
        if(cardAvailable) {
            drawCardsValue();
        }
        g.setColor(tmp);
    }

    private void drawCardsValue() {
        drawCardValue(this.cardsCors[0], this.cardsCors[1], cards[0], cards[1], g);
        drawCardValue(this.cardsCors[2], this.cardsCors[3], cards[2], cards[3], g);
    }

    // do be used by board cards
    // TODO dealer
    public static void drawCardValue(int x, int y, char c1, char c2, Graphics g) {
        Color oldColor = g.getColor();
        Font oldFont = g.getFont();
        Font font = new Font("Verdana", Font.BOLD + Font.ITALIC, 32);
        switch (c2) {
            case 's':
                g.setColor(Color.BLACK);
                break;
            case 'h':
                g.setColor(Color.RED);
                break;
            case 'd':
                g.setColor(Color.BLUE);
                break;
            case 'c':
                g.setColor(Color.GREEN);
                break;
            default:
                log.error("cards:" + c1+c2 +" NA");
                g.setColor(Color.yellow);
                break;
        }
        g.setFont(font);
        g.drawString(c1+"",x,y+positionUtils.getCardHeight()*3/4);
        g.setColor(oldColor);
        g.setFont(oldFont);
    }

    public void doAction(Action action) {
//        System.out.println("do action:" + action);
        log.debug("do action:" + action);
        switch (action.type) {
            case POST:
                doPostAction(action);
                break;
            case BET:
                doBetActoin(action);
                break;
            case CHECK:
                doCheckAction(action);
                break;
            case CALL:
                doCallAction(action);
                break;
            case RAISE:
                doRaiseAction(action);
                break;
            case FOLD:
                doFoldAction(action);
                break;
            default:
                break;
        }
//        g.setColor(Color.RED);
//        g.drawString("test",betChipCors[0],betChipCors[1]);
    }

//    public void cleanBetChip() {
//        this.cleanBetChip();
//    }

    private void drawCards() {
        g.setColor(Color.white);
        int[] cors = this.cardsCors;
        g.fillRect(cors[0], cors[1], positionUtils.getCardWidth(), positionUtils.getCardHeight());
        g.fillRect(cors[2], cors[3], positionUtils.getCardWidth(), positionUtils.getCardHeight());
    }

    private void cleanCards() {
        g.setColor(GifConstants.DEFAULT_TABLE_COLOR);
        int[] cors = this.cardsCors;
        g.fillRect(cors[0], cors[1], positionUtils.getCardWidth(), positionUtils.getCardHeight());
        g.fillRect(cors[2], cors[3], positionUtils.getCardWidth(), positionUtils.getCardHeight());
    }

    private void doBetActoin(Action action) {
        doPostAction(action);
    }

    private void doCallAction(Action action) {
        doPostAction(action);
    }

    private void doCheckAction(Action action) {
        System.out.println("draw check");
        drawCheck(Color.WHITE);

    }

    private void drawCheck(Color c) {
        Color tc = g.getColor();
        g.setColor(c);
        g.drawString("CHECK", betChipCors[0], betChipCors[1]);
        checked = true;
        g.setColor(tc);
    }

    private void doFoldAction(Action action) {
//        cleanBetChip();
        isFold = true;
        cleanCards();
        cleanBetChip();
    }

    private void doRaiseAction(Action action) {
        if(betChip!=null) {
            action.amount = action.amount.subtract(betChip);
        }
        doPostAction(action);
    }

    private void doPostAction(Action action) {
        updateHandChip(action.amount);
        updateBetChip(action.amount, action.type.toString());
        updatePot(action.amount);
    }

    private void updateHandChip(BigDecimal amount) {
        cleanHandChip();
        this.handChip = this.handChip.subtract(amount);
        drawHandChip();
    }

    private void cleanHandChip() {
        g.setColor(GifConstants.DEFAULT_PLAYER_COLOR);
        g.drawString(this.handChip.toString(), chipCors[0], chipCors[1]);
    }

    private void drawHandChip() {
        Color tmp = g.getColor();
//        String schip = this.handChip +"";
        g.setColor(GifConstants.DEFAULT_PLAYERINFO_COLOR);
        g.drawString(this.handChip.toString(), chipCors[0], chipCors[1]);
        g.setColor(tmp);
    }

    private void updateBetChip(BigDecimal amount, String actionName) {
        cleanBetChip();
        if (betChip == null) {
            this.betChip = amount;
        } else {
            this.betChip = this.betChip.add(amount);
        }
        this.actionName = actionName;
        drawBetChip();
    }

    private void cleanBetChip() {
        if(this.betChip != null) {
            Color c = g.getColor();
            g.setColor(GifConstants.DEFAULT_TABLE_COLOR);
            g.drawString(this.actionName + ":" + this.betChip.toString(), betChipCors[0], betChipCors[1]);
            g.setColor(c);
        } else if(checked) {
            checked = false;
            drawCheck(GifConstants.DEFAULT_TABLE_COLOR);
        }
    }

    private void drawBetChip() {
//        cleanBetChip();
        if(betChipCors == null)
            betChipCors = positionUtils.getPlayerBetChipCoordinate(position);
        Color tmp = g.getColor();
        g.setColor(GifConstants.DEFAULT_PLAYERINFO_COLOR);
        g.drawString(this.actionName + ":" + this.betChip.toString(), betChipCors[0], betChipCors[1]);
        g.setColor(tmp);
    }

    private void updatePot(BigDecimal amount) {
        cleanPot();
        pot = pot.add(amount);
//        System.out.println("TODO:update pot:" + pot.toString());
        drawPot();
    }

    private void cleanPot() {
        g.setColor(GifConstants.DEFAULT_TABLE_COLOR);
        g.drawString(pot.toString(), potCors[0], potCors[1]);
    }

    private void drawPot() {
        g.setColor(Color.WHITE);
        g.drawString(pot.toString(), potCors[0], potCors[1]);
    }


    public void putButton() {
        int[] cors = positionUtils.getButtonCoordiante(position);
        Font f = g.getFont();
        Color c = g.getColor();
        Font ff = new Font("Verdana", Font.BOLD + Font.ITALIC, 16);
        g.setFont(ff);
        g.setColor(Color.red);
//        char button = 'D';
        String str = "D";
        g.drawString(str,cors[0],cors[1]);
        g.setFont(f);
        g.setColor(c);
//        System.out.println(f);
//        System.out.println(ff);
    }

    public void collectBetChip() {
        this.cleanBetChip();
        this.betChip = null;
    }
}
