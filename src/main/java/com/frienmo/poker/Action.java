package com.frienmo.poker;

import java.math.BigDecimal;

/**
 * Created by 4 on 2016-01-30.
 */
public class Action {

    /** action types */
    public enum Type {
        CHECK("check"),
        FOLD("fold"),
        RAISE("raise"),
        CALL("call"),
        BET("bet"),
        /** post blind, not ante */
        POST("blind"),
        MUCK("muck"),
        DOESNTSHOW("doesn't show"),
        SHOW("show"),
        DRAW("draw"),
        STANDPAT("stand pat"),
        /** uncalled bet returned to player */
        UNCALL("returned"),
        /** win */
        COLLECT("collect"),
        BRINGSIN("bring in"),
        /** posts ante */
        ANTE("ante");
        public String desc;
        Type(String desc) {

            this.desc = desc;
        }

        @Override
        public String toString() {
            return desc;
        }
    }

    public Action(Player player) {
        this.player = player;
    }

    public Action(Player player, Action.Type type) {
        this.player = player;
        this.type = type;
    }

    public Action(Player player, Action.Type type, BigDecimal amount) {
        this.player = player;
        this.type = type;
        this.amount = amount;
    }

    public Action(Player player, Action.Type type, BigDecimal amount, boolean isAllin) {
        this.player = player;
        this.type = type;
        this.amount = amount;
        this.allin = isAllin;
    }

    /** player performing the action */
    public final Player player;
    /** action type */
    public Action.Type type;
    /** amount put in pot - can be negative if getting a refund */
    public BigDecimal amount;
    /** this action put the player all in */
    public boolean allin;

    /**
     * return string rep of action
     */
    @Override
    public String toString () {
        String s = player.getName() + " " + type.desc;
        if (amount != null) {
            s += " " + amount;
        }
        if (allin) {
            s += " all in";
        }
        return s;
    }
}

