package com.frienmo.poker;

import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by 4 on 2016-02-11.
 */
public class PokerStarParser {

    public static final Logger log = Logger.getLogger(PokerStarParser.class);

    private static final String POST_SB = "posts small blind";
    private static final String POST_BB = "posts big blind";
    private static final String SITSOUT = "sits out";

    private static final String DIV_HOLE_CARDS = "*** HOLE CARDS ***";
    private static final String DIV_FLOP = "*** FLOP ***";
    private static final String DIV_TURN = "*** TURN ***";
    private static final String DIV_RIVER = "*** RIVER ***";
    private static final String DIV_SHOW_DOWN = "*** SHOW DOWN ***";
    private static final String DIV_SUMMARY = "*** SUMMARY ***";

    ArrayList<String> lines;
    HandHistory hh;
    int pointer = 0;
    BlockState block = BlockState.INIT;

    enum BlockState {
        INIT,PREFLOP,FLOP,TURN,RIVER,SHOWDOWN,SUMMARY
    }

    public HandHistory parse(ArrayList<String> lines) throws HHParserException {
        HandHistory hh = new HandHistory(Site.POKERSTARS,lines);
        this.lines = lines;
        this.hh = hh;
        parseFirstLine();
        ++pointer;
        parseSecondLine();
        ++pointer;
        parse();

        log.debug(hh.getPreflopActions());
        log.debug(hh.getFlopActions());
        return hh;
    }

    private void parse() throws HHParserException {
//        int i = 2;

        String line = this.lines.get(2);
        pointer = 2;
        while (pointer < lines.size()) {
//            log.debug(block + ":" + line);
            line = lines.get(pointer);
            pointer++;
            switch (block){
                case INIT:
                    parseInitState(line);
                    break;
                case SUMMARY:
                    parseSummary(line);
                    break;
                case SHOWDOWN:
                    parseShowDown(line);
                    break;
                default:
                    parseAction(line);
            }
//            ++pointer;
//            line = this.lines.get(pointer);
        }
//        this.playerNumber = nameMap.size();
//        log.debug(playerPosMap);
//        this.pointer = i;
    }

    private void parseShowDown(String line) {
        if(line.equals(DIV_SUMMARY)){
            this.block = BlockState.SUMMARY;
            return;
        }
        log.debug(block + " showdown:" + line);
        String[] sp = line.split(": ");
        if(sp.length == 2) {
            // parse show
            String name = sp[0];
            String s = sp[1];
            Player p = hh.getPlayer(name);
            if (p.isCardAvailable())
                return;
            String c = s.substring(7, 12);
            String c2 = c.substring(0,2) + c.substring(3,5);
            log.debug(c2);
            p.setCards(c2);
        } else {

        }
    }

    private void parseInitState(String line) throws HHParserException {
        if(line.equals(DIV_HOLE_CARDS)) {
            this.block = BlockState.PREFLOP;
            return;
        }

        if(line.contains(POST_BB)) {
//                System.out.println(line);
            parsePostBlind(line);
        } else if(line.contains(POST_SB)) {
//                System.out.println(line);
            parsePostBlind(line);
        } else if(line.contains(SITSOUT)) {
//                System.out.println(line);
        } else if(line.startsWith("Seat") && block == BlockState.INIT){
            // Seat ...
            //Seat 3: tangyves (€2.36 in chips)
            String strPos = line.substring(5,6);
            int pos = Integer.valueOf(strPos);
            int idx = line.indexOf("(");
            String playerName = line.substring(8,idx-1);
            String s = line.substring(idx +1, line.length());
            if (s.contains(".")) {
                int pidx = s.indexOf(".");
                String ss = s.substring(1,pidx + 3);
//                float f = Float.valueOf(ss);
//                int cents = (int) (f*100);
                BigDecimal initChip = new BigDecimal(ss);
                Player p = new Player(playerName, initChip, pos);
                hh.addPlayer(p);
            } else {
                log.debug(block);
                throw new HHParserException("Can's get chips in line:" + line);
            }
        }
    }

    private void parseAction(String line) {
        if(line.startsWith("Dealt to")) {
            parseDealtTo(line);
            return;
        }
        if(line.startsWith(DIV_FLOP)) {
            this.block = BlockState.FLOP;
            char[] flopCards = new char[6];
            flopCards[0] = line.charAt(14);
            flopCards[1] = line.charAt(15);
            flopCards[2] = line.charAt(17);
            flopCards[3] = line.charAt(18);
            flopCards[4] = line.charAt(20);
            flopCards[5] = line.charAt(21);
            this.hh.setFlop(flopCards);
            return;
        } else if(line.startsWith(DIV_TURN)) {
            this.block = BlockState.TURN;
            char[] turnCard = new char[2];
            turnCard[0] = line.charAt(25);
            turnCard[1] = line.charAt(26);
            this.hh.setTurn(turnCard);
            return;
        } else if(line.startsWith(DIV_SUMMARY)) {
            this.block = BlockState.SUMMARY;
            return;
        } else if(line.startsWith(DIV_RIVER)) {
            this.block = BlockState.RIVER;
            char[] riverCard = new char[2];
            riverCard[0] = line.charAt(29);
            riverCard[1] = line.charAt(30);
            this.hh.setRiver(riverCard);
            return;
        } else if(line.startsWith(DIV_SHOW_DOWN)) {
            this.block = BlockState.SHOWDOWN;
            return;
        }
//        log.debug(block + " action:" + line);
        int idx = line.indexOf(":");
        if(idx == -1) {
            // uncalled bet
            parseUncalledBet(line);
            return;
        }
        String pname = line.substring(0,idx);
        Player p = hh.getPlayer(pname);
        String s = line.substring(idx+2,line.length());
        String[] parts = s.split(" ");
        String act = parts[0];
        Action action;

        if(act.equals("folds") || act.equals("checks")) {
            if(act.equals("folds")) {
                action = new Action(p,Action.Type.FOLD);
                addAction(action);
                return;
            } else {
                action = new Action(p,Action.Type.CHECK);
                addAction(action);
                return;
            }
        } else if(act.equals("calls") || act.equals("bets")) {
            String money = parts[1];
            BigDecimal dchip = ParserUtils.removeCurrencySymbolToCents(money);
            boolean isAllin = line.endsWith("and is all-in");
            if(act.equals("calls")) {
                action = new Action(p,Action.Type.CALL,dchip,isAllin);
                addAction(action);
                return;
            } else {
                action = new Action(p,Action.Type.BET,dchip,isAllin);
                addAction(action);
                return;
            }
        } else if(act.equals("raises")) {
            String sm2 = parts[3];
            boolean isAllin = line.endsWith("and is all-in");
            BigDecimal raisedTo = ParserUtils.removeCurrencySymbolToCents(sm2);
            action = new Action(p, Action.Type.RAISE,raisedTo,isAllin);
            addAction(action);
            return;
        } else {
            log.warn("unparse action:" + line);
        }
    }

    private void addAction(Action action) {
        switch (block) {
            case PREFLOP:
                hh.addPreflopAction(action);
                break;
            case FLOP:
                hh.addFlopAction(action);
                break;
            case TURN:
                hh.addTurnAction(action);
                break;
            case RIVER:
                hh.addRiverAction(action);
                break;
            default:
                log.error("actions in illegal state");
        }
    }

    private void parseUncalledBet(String line) {
        log.debug("parse uncalled bet: " + line);
    }

    private void parseSummary(String line) {
        log.debug(block + " summary:" + line);
        if (line.startsWith("Total pot")) {
            String[] parts = line.split(" ");
            String pot = parts[2];
            hh.setTotalPot(ParserUtils.removeCurrencySymbolToCents(pot));
            if (parts.length > 4) {
                String rake = parts[5];
                hh.setRake(ParserUtils.removeCurrencySymbolToCents(rake));
            }
        } else if(line.startsWith("Board")) {

        } else if(line.startsWith("Seat")) {

        } else {
            log.error("cant parse summary line :" + line);
        }
     }

    private void parseDealtTo(String line) {
        int idx = line.indexOf("[");
        int l = line.length();
        String heroName = line.substring(9,l-8);
        Player hero = hh.getPlayer(heroName);
        String cards = line.substring(idx+1,idx+3) + line.substring(idx+4,idx+6);

        hero.setCards(cards);
    }

    private void parsePostBlind(String line) {
        Player p = getPlayerBefore(line);
        BigDecimal amount = getAmountAtLast(line);
        Action action = new Action(p);
        action.amount = amount;
        action.type = Action.Type.POST;
        hh.addPreflopAction(action);

    }

    private Player getPlayerBefore(String line) {
        int idx = line.indexOf(":");
        return hh.getNameMap().get(line.substring(0, idx));
    }

    private BigDecimal getAmountAtLast(String line) {
        String[] parts = line.split(" ");
        String s = parts[parts.length-1];
        return ParserUtils.removeCurrencySymbolToCents(s);
    }

    public int getPlayerNumber() {
        return hh.getPlayerNumber();
    }

    private void parseSecondLine() {
//        log.debug(line);
        String line = lines.get(pointer);
        int idx = line.indexOf("#");
        hh.setDealerPos(Integer.valueOf(line.substring(idx+1,idx+2)));
    }

    private void parseFirstLine() {
        // TODO check language,currency
//        System.out.println(Site.POKERSTARS.name());
//        System.out.println(Site.POKERSTARS.toString());
        //PokerStars Hand #147904702097:  Hold'em No Limit (€0.01/€0.02 EUR) - 2016/01/30 0:46:58 CET [2016/01/29 18:46:58 ET]
        String firstLine = hh.getOriginalText().get(0);
//        System.out.println(firstLine);
        if (firstLine.startsWith(Site.POKERSTARS.toString())) {
            int idx1 = firstLine.indexOf('#');
//            System.out.println(idx1);
            int idx2 = firstLine.indexOf(':');
//            System.out.println(idx2);
            String nbHH = firstLine.substring(idx1 + 1,idx2);
//            System.out.println(nbHH);
            hh.setInitIdBySite(nbHH);
            hh.setCurrency(ParserUtils.detectCurrency(firstLine));

            int idx3 = firstLine.indexOf('(');
            int idx4 = firstLine.indexOf('/');
            String sbStr = firstLine.substring(idx3 + 2,idx4);
            hh.setSmallBlind(Float.valueOf(sbStr));
            sbStr = firstLine.substring(idx4+2,idx4 + 6);
            hh.setBigBlind(Float.valueOf(sbStr));

            int idx5 = firstLine.indexOf('[');
            int idx6 = firstLine.indexOf(']');
            hh.setTime(firstLine.substring(idx5 + 1,idx6));
        }
    }
}
