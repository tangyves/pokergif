package com.frienmo.poker.exp;

import com.frienmo.poker.HandHistory;
import com.frienmo.poker.gif.AnimatedGifEncoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 4 on 2016-02-07.
 */
public class Exp {
    public static void main(String[] args) throws IOException {

//        bigDecimal();
        gif();
    }

    public static void gif() throws IOException {
        BufferedImage firstImage = ImageIO.read(new File("test1.gif"));
        OutputStream output = new FileOutputStream(new File("test.gif"));

        AnimatedGifEncoder e = new AnimatedGifEncoder();
        e.start(output);
        e.setDelay(5000);   // 1 frame per sec
        for(int i = 1;i <=7; i++) {
            e.setDelay(500);
            BufferedImage image = ImageIO.read(new File("test"+i+".gif"));
            e.addFrame(image);
        }
//        e.addFrame(image2);


//        GifSequenceWriter writer = new GifSequenceWriter(output, firstImage.getType(), 500, true);
//        writer.writeToSequence(firstImage);
//        for(int i=2; i<=7; i++)
//        {
//            BufferedImage nextImage = ImageIO.read(new File("test"+i+".gif"));
//            writer.writeToSequence(nextImage);
//        }
//        writer.close();
//        output.close();
    }

    public static void bigDecimal() {
        String s = "2.01";
        BigDecimal bg = new BigDecimal(s);
        BigDecimal bg2 = new BigDecimal("0.02");

        System.out.println(bg.subtract(bg2));
        BigDecimal a = new BigDecimal(0);
        a.add(bg2);
        System.out.println(a.add(bg2));
    }

    public static void doubleMath() {
        float d1 = 2.01f;
        int i = (int) (d1*100);
        System.out.println(i);
//        double d2 = 0.01d;
//        Double d = d2 - d1;
//        BigDecimal value = new BigDecimal(d2-d1);
//        value.setScale(2);
//        float f1 = 2.01f;
//        float f2 = 0.01f;
//        System.out.println("bigd:" + value);
//        System.out.println(f2-f1);
//        System.out.println(d2-d1);
    }

    public static void isIntFinal() {
        Map<Integer, int[]> map = new HashMap<>();
        int[] arr = new int[2];
        arr[0] = 1;
        arr[1] = 2;
        map.put(1,arr);
        arr[0] = 3;
        arr[1] = 4;
        map.put(2,arr);
        System.out.println(map.get(1)[0]);

        HandHistory hh = new HandHistory();
        char[] cards = new char[2];
        cards[0] = 'a';
        cards[1] = 's';
        hh.setRiver(cards);
        cards[0] = 'k';
        System.out.println(hh.getRiver());
    }
}
