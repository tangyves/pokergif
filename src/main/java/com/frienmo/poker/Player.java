package com.frienmo.poker;

import java.math.BigDecimal;

/**
 * Created by 4 on 2016-01-29.
 */
public class Player {
    protected String name;
    private BigDecimal initChip;
    protected String note;
    protected int position;

    protected char[] cards = new char[4];
    protected boolean cardAvailable = false;

    public Player(String name, BigDecimal initChip, int position) {
        this.name = name;
        this.initChip = initChip;
        this.position = position;
    }

    public Player(String name, String note, BigDecimal initChip, int position) {
        this.name = name;
        this.initChip = initChip;
        this.note = note;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setInitChip(BigDecimal initChip) {
        this.initChip = initChip;
    }

    public BigDecimal getInitChip() {
        return initChip;
    }


    public int getPosition() {
        return position;
    }

    public char[] getCards() {
        return cards;
    }

    public void setCards(String cards) {
//        this.cards = cards;
        this.cards = cards.toCharArray();
        cardAvailable = true;
    }

    public boolean isCardAvailable() {
        return cardAvailable;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name=" + name + ", initChip=" + initChip +"]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        if (name != null ? !name.equals(player.name) : player.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
